/* 
 * The host ember app doesn't see imports from /addons, so library imports there
 * must also be imported here.
 * See https://github.com/ef4/ember-browserify#using-ember-browserify-in-addons
 */
import s from "npm:underscore.string";
import moment from "npm:moment";
export { default } from 'ember-kii-devportal-metrics/components/kii-devportal-metrics/component';