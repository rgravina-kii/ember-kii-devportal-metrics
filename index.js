/* jshint node: true */
'use strict';

module.exports = {
  name: 'ember-kii-devportal-metrics',
  included: function(app) {
    app.import('bower_components/underscore/underscore.js');
    app.import('bower_components/underscore.string/dist/underscore.string.js');
    app.import('bower_components/moment/moment.js');
  },
  isDevelopingAddon: function() {
    return true;
  }
};
