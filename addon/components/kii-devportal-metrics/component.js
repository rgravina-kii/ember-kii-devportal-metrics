import Ember from 'ember';
import layout from './template';
import _ from "npm:underscore";
import s from "npm:underscore.string";
import moment from "npm:moment";

class GraphContext {
  constructor(endpointBaseURL, appId, appCreatedAt, metric, options = {}) {
    this.endpointBaseURL = endpointBaseURL;
    this.appId = appId;
    this.appCreatedAt = appCreatedAt;
    this.metric = metric;
    this.granularity = options.granularity;
    this.from = options.from != null ? options.from : this.appCreatedAt;
    this.to = options.to != null ? options.to : moment().format();
    this.cache = options.cache != null ? options.cache : true;
  }

  boundaries() {
    return _.extend(this, {from: this.appCreatedAt, to: moment().format()});
  }

  daily() {
    return _.extend(this, {granularity: 'day'});
  }
}

class Graph {
  constructor(endpointBaseURL, appId, appCreatedAt, metric, options = {}) {
    this.context = new GraphContext(endpointBaseURL, appId, appCreatedAt, metric, options);
    const offsetsString = this._storage().getItem(this._offsetKeyName());
    this.offsets = offsetsString != null ? JSON.parse(offsetsString) : null;
  }

  fetch() {
    const combiner = new MetricCombiner(this.context);
    return new Ember.RSVP.Promise((resolve) => {
      combiner.fetch().then((actual) => {
        this.context.from = actual.from;
        this.context.to = actual.to;
        this.offset().then((offset) => {
          /*
           * offset the total, and add it to the data object so that
           * the GraphView can offet the data in _addSeries.
           * It is not done here because only cumulative values should be
           * offset.
           */
          actual.total = actual.total + offset;
          
          /*
           * * 3scale also returns the start date at the beggining of the
           * time period (e.g. 2014-09-01T00:00:00Z) but the value
           * actually represents the total at the end of the time period
           * (e.g. 2014-09-01T23:59:59Z). So, we need to round it up to the
           * end of the time period.
           * * Replace the time in the data so GraphView will use the correct
           * time at the end of the period
           * * Due to import order, can't require TimeUtils outside the class
           */
          // Note: this returns a moment object but shouldn't matter
          actual.from = this.context.from = this._roundUp(actual.from, actual.granularity);
          resolve(_.extend({offset: offset}, actual));
        });
      });
    });
  }

  offset() {
    return new Ember.RSVP.Promise((resolve) => {
      if (this.context.from == null) {
        resolve(0);
        return;        
      }
      // The offset should be the total of the day before the start
      // date for the graph
      const daysDifference = moment(this.context.from).diff(this.context.appCreatedAt, 'days');
      if (daysDifference <= 0){
        resolve(0);
        return;
      }
      else {
        this._setupCumulativeOffsets(daysDifference).then(
          (offsets) => {
            resolve(offsets[daysDifference]);
          }
        );
      }
    });
  }

  _roundUp(time, granularity) {
    // Add a day/month/year etc. and go back one second
    // This will convert e.g. 2014-09-01T00:00:00 to 2014-09-01T23:59:59
    return moment(time).add(1, granularity).subtract(1, 'seconds');
  }

  _offsetKeyName() {
    return `devpo-v2-graph-offsets-${this.context.appId}-${this.context.metric}`;
  }
  _storage() {
    // change to localStorage for local storage
    return window.sessionStorage;
  }

  _setupCumulativeOffsets(daysDifference) {
    return new Ember.RSVP.Promise((resolve) => {
      // If the offest for the selected day is available, just return it
      if (this.offsets != null && this.offsets[daysDifference] != null) {
        resolve(this.offsets);
      } else {
        // Get a daily history of all values for this metric.
        // As it is expensive, store it in the session.
        const combiner = new MetricCombiner(this.context.boundaries().daily());
        combiner.fetch().then((data) => {
          // Sum values to make them cumulative. This is important, so we can
          // quickly get the total of the day before to offset a graphs values
          // later
          const newValues = [data.values[0]];
          for (let i = 1; i < data.values.length-1; i++) {
            newValues.push(newValues[i-1] + data.values[i]);
          }
          this.offsets = newValues;
          //Now, store offsets in the session and return
          this._storage().setItem(this._offsetKeyName(), JSON.stringify(this.offsets));
          resolve(this.offsets);
        });
      }
    });
  }
}

class MetricCombiner {
  constructor(context) {
    this.context = context;
  }

  fetch() {
    const requester = new MetricRequester(this.context);
    if (s.endsWith(this.context.metric, '-created') || s.endsWith(this.context.metric, '-deleted')) {
      requester.fetch();
    } else {
      return new Ember.RSVP.Promise(function(resolve){
        requester.fetch().then(function (metrics) {
          const created = metrics[0];
          const deleted = metrics[1];
          const actual = {
            from: created.from,
            to: created.to,
            granularity: created.granularity,
            total: created.total - deleted.total,
            latest: created.latest === true && deleted.latest === true,
            values: _.map(created.values,
              function (value, index) {
                return (value - deleted.values[index]);
              }
            )
          };
          resolve(actual);
        });
      });
    }
  }
}

class MetricRequester {
  constructor(context) {
    this.context = context;
  }

  fetch() {
    const url = `${this.context.endpointBaseURL}/v2ext/apps/${this.context.appId}/metrics`;
    return Ember.$.ajax({
      type: 'post',
      url: url,
      data: {
        metrics: [`${this.context.metric}-created`, `${this.context.metric}-deleted`],
        from: this.context.from,
        to: this.context.to,
        granularity: this.context.granularity,
        cache: this.context.cache
      }
    });
  }
}


class ChartistGraph {
  constructor(data, options={}) {
    this.metricsData = data;
    this.options = _.extend({
      cumulative: true,
    }, options);
    // convert metrics data to chartist series and labels
    this.chartData = {
      labels: _.map(data.values, function(){return "hello";}),
      series: [this.offsetValues()]
    };
    this.chartOptions = {
      lineSmooth: false,
      axisX: {
        showGrid: false
      }
    };
  }

  offsetValues() {
    const data = this.metricsData;
    if (this.options.cumulative === true) {
      const newValues = [data.values[0]];
      for (let i = 1; i < data.values.length; i++) {
        newValues.push(newValues[i-1] + data.values[i]);
      }
      return newValues;  
    } else {
      return data.values; 
    }
  }

  chartistData() {
    return {
      data: this.chartData,
      options: this.chartOptions
    };
  }
}

export default Ember.Component.extend({
  layout: layout,
  cumulative: true,
  model: function() {
    return new Ember.RSVP.Promise((resolve) => {
      /* 
       * The v2ext Metrics API returns an array of results matching the
       * order of metrics in the request. We have to merge created and
       * deleted into totals for the day, and also to display them on a
       * graph we need offset them by the total up to the day before.
       */
      const graph = new Graph(
        this.get('endpointBaseURL'),
        this.get('appId'),
        this.get('appCreatedAt'),
        this.get('metric'), {
          granularity: this.get('granularity'),
          from: this.get('from'),
          to: this.get('to'),
          cached: this.get('cached')
        }
      );
      graph.fetch().then((data) => {
        if (!(this.get('isDestroyed') || this.get('isDestroying'))) {
          this.set('metrics', data);
          const graph = new ChartistGraph(data, {
            cumulative: this.get('cumulative')
          });
          resolve(graph.chartistData());
        }
      });
    });
  },
  didInsertElement: function(){
    this.model().then((data) => {
      this.setProperties(data);
    });
  },
  observeCumulative: function() {
    const graph = new ChartistGraph(this.get('metrics'), {
      cumulative: this.get('cumulative')
    });
    this.setProperties(graph.chartistData());
  }.observes('cumulative')
});
