import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('kii-devportal-metrics', 'Integration | Component | kii devportal metrics', {
  integration: true
});

test('it renders', function(assert) {
  assert.expect(2);

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{kii-devportal-metrics}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#kii-devportal-metrics}}
      template block text
    {{/kii-devportal-metrics}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
